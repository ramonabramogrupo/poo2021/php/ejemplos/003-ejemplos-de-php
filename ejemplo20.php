<?php
    const MIN=1;
?>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            // comprobando si se pulso BUTTON
            //if(!isset($_GET["numero"])){ 
            //if(empty($_GET)){
            if(!$_GET){ 
            
        ?>
        
        <form method="get">
            <div>
            <label>Introduce el numero</label>
            <input type="number" name="numero" min="2" > 
            </div>
            <button>Mostrar torre</button>
        </form>
            
        <?php
            }else{
                // aqui entra cuando he pulsado el BUTTON
                $numero=$_GET["numero"];
                echo "<ul>"; // fuera de los bucles. Esto solo se ejecuta 1 vez
                for($contador=MIN;$contador<$numero;$contador++){ // este bucle le controla numero
                    echo "<li>"; // esto se ejecuta al principio de cada iteracion del bucle 1
                    for($contador1=MIN;$contador1<=$contador;$contador1++){ //este bucle le controla contador
                        echo "{$contador1} "; // esto se ejecuta en cada iteracion del bucle 2
                    }
                    echo "</li>"; // esto se ejecuta al final de cada iteracion del bucle 1
                }
                echo "</ul>";
            }
        ?>
    </body>
</html>
